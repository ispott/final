<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1" unitdist="mm" unit="mm" style="lines" multiple="1" display="yes" altdistance="0.1" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:39417/1" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground Symbol)&lt;/h3&gt;</description>
<pin name="3.3V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="0" y="-1.778" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND2" urn="urn:adsk.eagle:component:39442/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground style)&lt;/h3&gt;
&lt;p&gt;Ground supply with a traditional "earth ground" symbol.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="battery" urn="urn:adsk.eagle:library:109">
<description>&lt;b&gt;Lithium Batteries and NC Accus&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="10MM_SM_COIN_CELL_CLIP" urn="urn:adsk.eagle:footprint:4563/1" library_version="3">
<description>Battery cell clip for CR927&lt;p&gt;
Source: KEYSTONE ELECTRONICS CORP.&lt;br&gt;
www.keyelco.com, ASTORIA, N.Y. 11105-2017&lt;br&gt;
PART ANME 10mm SM COIN CELl CLIP&lt;br&gt;
DWG NO. 3030</description>
<smd name="+@1" x="-6.6" y="0" dx="2.9" dy="3.5" layer="1"/>
<smd name="+@2" x="6.6" y="0" dx="2.9" dy="3.5" layer="1"/>
<smd name="-" x="0" y="0" dx="3.96" dy="3.96" layer="1"/>
<wire x1="-7.9499" y1="1.7507" x2="-5.1086" y2="1.7507" width="0.2" layer="51"/>
<wire x1="-5.1086" y1="1.7507" x2="-4.7642" y2="1.8942" width="0.2" layer="51" curve="45.239625"/>
<wire x1="-4.7642" y1="1.8942" x2="-3.157" y2="3.731" width="0.2" layer="21"/>
<wire x1="-3.157" y1="3.731" x2="-2.4969" y2="4.018" width="0.2" layer="21" curve="-50.631042"/>
<wire x1="-2.4969" y1="4.018" x2="0.0287" y2="4.018" width="0.2" layer="21"/>
<wire x1="-5.1947" y1="1.8081" x2="-5.1947" y2="2.1525" width="0.2" layer="51"/>
<wire x1="-5.1947" y1="2.1525" x2="-5.1373" y2="2.3247" width="0.2" layer="51" curve="-36.869898"/>
<wire x1="-5.1373" y1="2.3247" x2="-4.8216" y2="2.9848" width="0.2" layer="21"/>
<wire x1="-4.8216" y1="2.9848" x2="-4.879" y2="3.5875" width="0.2" layer="21" curve="62.000554"/>
<wire x1="-4.879" y1="3.5875" x2="-5.1373" y2="3.9606" width="0.2" layer="21"/>
<wire x1="-7.9499" y1="1.7507" x2="-7.9499" y2="-1.6933" width="0.2" layer="51"/>
<wire x1="-7.9499" y1="-1.6933" x2="-4.5346" y2="-1.6933" width="0.2" layer="51"/>
<wire x1="-4.5346" y1="-1.6933" x2="-4.3624" y2="-2.2386" width="0.2" layer="21" curve="-144.948737"/>
<wire x1="-4.3624" y1="-2.2386" x2="-4.8216" y2="-2.6978" width="0.2" layer="21"/>
<wire x1="-4.8216" y1="-2.6978" x2="-3.0996" y2="-4.5346" width="0.2" layer="21"/>
<wire x1="-3.0996" y1="-4.5346" x2="-2.1812" y2="-3.6449" width="0.2" layer="21"/>
<wire x1="-2.1812" y1="-3.6449" x2="-1.8942" y2="-3.5301" width="0.2" layer="21" curve="-44.578282"/>
<wire x1="-5.2521" y1="-1.6072" x2="-5.2521" y2="1.6933" width="0.2" layer="51"/>
<wire x1="-2.0377" y1="-1.435" x2="-2.0377" y2="1.6359" width="0.2" layer="51"/>
<wire x1="-2.0377" y1="1.6359" x2="-1.8081" y2="1.8655" width="0.2" layer="51" curve="-90"/>
<wire x1="-1.8081" y1="1.8655" x2="-1.722" y2="1.8655" width="0.2" layer="51"/>
<wire x1="-1.722" y1="1.8655" x2="-1.5211" y2="1.6646" width="0.2" layer="51" curve="-90"/>
<wire x1="-1.5211" y1="1.6359" x2="-1.5211" y2="-0.6888" width="0.2" layer="51"/>
<wire x1="-1.5211" y1="-0.6888" x2="-1.0619" y2="-1.148" width="0.2" layer="51" curve="90"/>
<wire x1="-1.5498" y1="-1.9229" x2="-2.0377" y2="-1.435" width="0.2" layer="51" curve="-90"/>
<wire x1="7.9499" y1="1.7507" x2="5.1086" y2="1.7507" width="0.2" layer="51"/>
<wire x1="5.1086" y1="1.7507" x2="4.7642" y2="1.8942" width="0.2" layer="51" curve="-45.239625"/>
<wire x1="4.7642" y1="1.8942" x2="3.157" y2="3.731" width="0.2" layer="21"/>
<wire x1="3.157" y1="3.731" x2="2.4969" y2="4.018" width="0.2" layer="21" curve="50.631042"/>
<wire x1="2.4969" y1="4.018" x2="-0.0287" y2="4.018" width="0.2" layer="21"/>
<wire x1="5.1947" y1="1.8081" x2="5.1947" y2="2.1525" width="0.2" layer="51"/>
<wire x1="5.1947" y1="2.1525" x2="5.1373" y2="2.3247" width="0.2" layer="51" curve="36.869898"/>
<wire x1="5.1373" y1="2.3247" x2="4.8216" y2="2.9848" width="0.2" layer="21"/>
<wire x1="4.8216" y1="2.9848" x2="4.879" y2="3.5875" width="0.2" layer="21" curve="-62.000554"/>
<wire x1="4.879" y1="3.5875" x2="5.1373" y2="3.9606" width="0.2" layer="21"/>
<wire x1="7.9499" y1="1.7507" x2="7.9499" y2="-1.6933" width="0.2" layer="51"/>
<wire x1="7.9499" y1="-1.6933" x2="4.5346" y2="-1.6933" width="0.2" layer="51"/>
<wire x1="4.5346" y1="-1.6933" x2="4.3624" y2="-2.2386" width="0.2" layer="21" curve="144.948737"/>
<wire x1="4.3624" y1="-2.2386" x2="4.8216" y2="-2.6978" width="0.2" layer="21"/>
<wire x1="4.8216" y1="-2.6978" x2="3.0996" y2="-4.5346" width="0.2" layer="21"/>
<wire x1="3.0996" y1="-4.5346" x2="2.1812" y2="-3.6449" width="0.2" layer="21"/>
<wire x1="2.1812" y1="-3.6449" x2="1.8942" y2="-3.5301" width="0.2" layer="21" curve="44.578282"/>
<wire x1="1.8942" y1="-3.5301" x2="-1.8942" y2="-3.5301" width="0.2" layer="21"/>
<wire x1="5.2521" y1="-1.6072" x2="5.2521" y2="1.6933" width="0.2" layer="51"/>
<wire x1="2.0377" y1="-1.435" x2="2.0377" y2="1.6359" width="0.2" layer="51"/>
<wire x1="2.0377" y1="1.6359" x2="1.8081" y2="1.8655" width="0.2" layer="51" curve="90"/>
<wire x1="1.8081" y1="1.8655" x2="1.722" y2="1.8655" width="0.2" layer="51"/>
<wire x1="1.722" y1="1.8655" x2="1.5211" y2="1.6646" width="0.2" layer="51" curve="90"/>
<wire x1="1.5211" y1="1.6359" x2="1.5211" y2="-0.6888" width="0.2" layer="51"/>
<wire x1="1.5211" y1="-0.6888" x2="1.0619" y2="-1.148" width="0.2" layer="51" curve="-90"/>
<wire x1="1.0619" y1="-1.148" x2="-1.0619" y2="-1.148" width="0.2" layer="51"/>
<wire x1="-1.5498" y1="-1.9229" x2="1.5498" y2="-1.9229" width="0.2" layer="51"/>
<wire x1="1.5498" y1="-1.9229" x2="2.0377" y2="-1.435" width="0.2" layer="51" curve="90"/>
<circle x="-6.601" y="0.0574" radius="0.83279375" width="0.2" layer="51"/>
<circle x="6.601" y="0.0574" radius="0.83279375" width="0.2" layer="51"/>
<rectangle x1="-0.0861" y1="2.0664" x2="0.1435" y2="3.444" layer="21"/>
<rectangle x1="-0.6601" y1="2.6404" x2="0.7175" y2="2.87" layer="21"/>
<text x="-6.66375" y="4.62751875" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-4.48255" y="-6.43935" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="10MM_SM_COIN_CELL_CLIP" urn="urn:adsk.eagle:package:4612/1" type="box" library_version="3">
<description>Battery cell clip for CR927
Source: KEYSTONE ELECTRONICS CORP.
www.keyelco.com, ASTORIA, N.Y. 11105-2017
PART ANME 10mm SM COIN CELl CLIP
DWG NO. 3030</description>
<packageinstances>
<packageinstance name="10MM_SM_COIN_CELL_CLIP"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="1V2" urn="urn:adsk.eagle:symbol:4515/1" library_version="3">
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-2.54" width="0.4064" layer="94"/>
<text x="-1.27" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="-" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10MM_SM_COIN_CELL_CLIP" urn="urn:adsk.eagle:component:4674/2" prefix="G" library_version="3">
<description>Battery cell clip for CR927&lt;p&gt;
Source: KEYSTONE ELECTRONICS CORP.&lt;br&gt;
www.keyelco.com, ASTORIA, N.Y. 11105-2017&lt;br&gt;
PART ANME 10mm SM COIN CELl CLIP&lt;br&gt;
DWG NO. 3030</description>
<gates>
<gate name="G$1" symbol="1V2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="10MM_SM_COIN_CELL_CLIP">
<connects>
<connect gate="G$1" pin="+" pad="+@1 +@2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:4612/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LM2576T">
<packages>
<package name="T05A_TEX">
<pad name="1" x="0" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="2" x="1.7018" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="3" x="3.4036" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="4" x="5.1054" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="5" x="6.8072" y="0" drill="1.0922" diameter="1.6002"/>
<wire x1="-1.9812" y1="-1.9812" x2="8.7884" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="-1.9812" x2="8.7884" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="2.9972" x2="-1.9812" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="2.9972" x2="-1.9812" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="0" x2="-3.9624" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.9624" y1="0" x2="-3.556" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="-0.254" x2="0" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="-1.8288" x2="8.6868" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="-1.8288" x2="8.6868" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="2.8448" x2="-1.8796" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="2.8448" x2="-1.8796" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="-1.1684" y1="0" x2="-1.5748" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.5748" y1="0" x2="-1.1684" y2="0" width="0" layer="51" curve="-180"/>
<text x="0.127" y="-0.127" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="1.6764" y="-0.127" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="LM2576T-5_0_1">
<pin name="VIN" x="2.54" y="0" length="middle" direction="pwr"/>
<pin name="OUT" x="2.54" y="-2.54" length="middle" direction="pwr"/>
<pin name="GND" x="63.5" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="FB" x="63.5" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="~ON~/OFF" x="63.5" y="0" length="middle" direction="in" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="58.42" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-10.16" x2="58.42" y2="5.08" width="0.1524" layer="94"/>
<wire x1="58.42" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="28.2956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="27.6606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM2576T-ADJ/LF03" prefix="U">
<gates>
<gate name="A" symbol="LM2576T-5_0_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T05A_TEX">
<connects>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="3"/>
<connect gate="A" pin="OUT" pad="2"/>
<connect gate="A" pin="VIN" pad="1"/>
<connect gate="A" pin="~ON~/OFF" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="296-35127-5-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-LM2576T-ADJ/LF03-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LM2576T-ADJ/LF03" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Lab6_Library">
<packages>
<package name="1572-1039-CIN">
<pad name="P$1" x="0" y="0" drill="1"/>
<pad name="P$2" x="2" y="0" drill="1"/>
<circle x="1" y="0" radius="5" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0.6" y2="0" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="0.6" y2="1" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="1" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="0" x2="2" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="0" x2="2" y2="0" width="0.127" layer="21"/>
<text x="0" y="0.8" size="0.6096" layer="21">+</text>
</package>
<package name="1N5820">
<pad name="P$1" x="0" y="0" drill="1.3"/>
<pad name="P$2" x="9" y="0" drill="1.3"/>
<wire x1="0" y1="0" x2="4" y2="0" width="0.127" layer="21"/>
<wire x1="4" y1="0" x2="4" y2="2" width="0.127" layer="21"/>
<wire x1="4" y1="2" x2="6" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="4" y2="-2" width="0.127" layer="21"/>
<wire x1="4" y1="-2" x2="4" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="2" width="0.127" layer="21"/>
<wire x1="6" y1="2" x2="5.3" y2="2" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="-2" width="0.127" layer="21"/>
<wire x1="6" y1="-2" x2="6.6" y2="-2" width="0.127" layer="21"/>
<wire x1="6.6" y1="-2" x2="6.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="9" y2="0" width="0.127" layer="21"/>
<wire x1="5.3" y1="2" x2="5.3" y2="1.5" width="0.127" layer="21"/>
<text x="2.032" y="0.635" size="1.27" layer="21">+</text>
<text x="7.366" y="0.762" size="1.27" layer="21">-</text>
</package>
<package name="M10144">
<pad name="P$1" x="0" y="0" drill="0.6"/>
<pad name="P$2" x="7.5" y="0" drill="0.6"/>
<wire x1="0" y1="0" x2="1.5" y2="0" width="0.127" layer="21"/>
<wire x1="7.5" y1="0" x2="6.7" y2="0" width="0.127" layer="21"/>
<wire x1="1.5" y1="0" x2="2.4" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="2.4" y1="0.9" x2="2.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="2.5" y1="0.9" x2="3.2" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="3.2" y1="0.2" x2="3.2" y2="0" width="0.127" layer="21"/>
<wire x1="3.2" y1="0" x2="4.1" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="4.1" y1="0.9" x2="4.2" y2="0.9" width="0.127" layer="21"/>
<wire x1="4.2" y1="0.9" x2="4.8" y2="0.3" width="0.127" layer="21" curve="-90"/>
<wire x1="4.8" y1="0.3" x2="4.8" y2="0" width="0.127" layer="21"/>
<wire x1="4.8" y1="0" x2="5.7" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="5.7" y1="0.9" x2="6" y2="0.9" width="0.127" layer="21"/>
<wire x1="6" y1="0.9" x2="6.7" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="6.7" y1="0.2" x2="6.7" y2="0" width="0.127" layer="21"/>
</package>
<package name="1572-1055-COUT">
<pad name="P$1" x="0" y="0" drill="0.9"/>
<pad name="P$2" x="8" y="0" drill="0.9"/>
<circle x="4" y="0" radius="8" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.4" y2="0" width="0.127" layer="21"/>
<wire x1="1.5" y1="0" x2="3" y2="0" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="3" y2="-1" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="3" y2="1" width="0.127" layer="21"/>
<wire x1="4.6" y1="-1" x2="3.6" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="3.6" y1="0" x2="4.6" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="3.6" y1="0" x2="8.1" y2="0" width="0.127" layer="21"/>
<text x="1.5" y="0.4" size="1.27" layer="21">+</text>
</package>
<package name="CPF11K0000FKEE6-R1">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="7" y="0" drill="0.7"/>
<wire x1="5.715" y1="0" x2="7" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.715" y2="0" width="0.127" layer="21"/>
</package>
<package name="CF18JT6K20CT">
<pad name="P$1" x="0" y="0" drill="0.5"/>
<pad name="P$2" x="4" y="0" drill="0.5"/>
<wire x1="0" y1="0" x2="0.5" y2="0" width="0.127" layer="21"/>
<wire x1="0.5" y1="0" x2="1" y2="1" width="0.127" layer="21"/>
<wire x1="1" y1="1" x2="1.5" y2="-1" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="2" y1="1" x2="2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1" x2="3" y2="1" width="0.127" layer="21"/>
<wire x1="3" y1="1" x2="3.4" y2="0" width="0.127" layer="21"/>
<wire x1="3.4" y1="0" x2="4" y2="0" width="0.127" layer="21"/>
</package>
<package name="NF123G-302">
<pad name="P$1" x="0" y="0" drill="1"/>
<pad name="P$2" x="12.5" y="0" drill="1"/>
<wire x1="0" y1="0" x2="2" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="4" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="4" y2="2" width="0.127" layer="21"/>
<wire x1="12.5" y1="0" x2="10.5" y2="0" width="0.127" layer="21"/>
<wire x1="10.5" y1="0" x2="10.5" y2="2" width="0.127" layer="21"/>
<wire x1="10.5" y1="2" x2="9" y2="2" width="0.127" layer="21"/>
<wire x1="10.5" y1="0" x2="10.5" y2="-2" width="0.127" layer="21"/>
<wire x1="10.5" y1="-2" x2="9" y2="-2" width="0.127" layer="21"/>
<circle x="6.5" y="0" radius="10.01798125" width="0.127" layer="21"/>
<circle x="6.5" y="0" radius="3.11448125" width="0.127" layer="21"/>
<text x="5.8" y="-0.6" size="1.27" layer="21">M</text>
</package>
</packages>
<symbols>
<symbol name="1572-1039-CIN">
<wire x1="0" y1="0" x2="2" y2="0" width="0.254" layer="94"/>
<wire x1="2" y1="0" x2="2" y2="2" width="0.254" layer="94"/>
<wire x1="2" y1="-2" x2="2" y2="0" width="0.254" layer="94"/>
<wire x1="5" y1="-2" x2="5" y2="2.1" width="0.254" layer="94" curve="-180"/>
<text x="0.5" y="0.4" size="1.27" layer="94">+</text>
<pin name="P$1" x="-3" y="0" length="middle"/>
<pin name="P$2" x="8" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="1N5820">
<wire x1="0" y1="0" x2="3" y2="0" width="0.254" layer="94"/>
<wire x1="3" y1="0" x2="3" y2="2" width="0.254" layer="94"/>
<wire x1="3" y1="0" x2="3" y2="-2" width="0.254" layer="94"/>
<wire x1="3" y1="-2" x2="5" y2="0" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="3" y2="2" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="5" y2="2" width="0.254" layer="94"/>
<wire x1="5" y1="2" x2="4.3" y2="2" width="0.254" layer="94"/>
<wire x1="4.3" y1="2" x2="4.3" y2="1.6" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="5" y2="-2" width="0.254" layer="94"/>
<wire x1="5" y1="-2" x2="5.8" y2="-2" width="0.254" layer="94"/>
<wire x1="5.8" y1="-2" x2="5.8" y2="-1.3" width="0.254" layer="94"/>
<pin name="P$1" x="-2" y="0" length="middle"/>
<pin name="P$2" x="10" y="0" length="middle" rot="R180"/>
<text x="1.5" y="0.4" size="1.27" layer="94">+</text>
<text x="5.7" y="0.5" size="1.27" layer="94">-</text>
</symbol>
<symbol name="M10144">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="3" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="3" y1="0" x2="5" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="5" y1="0" x2="7" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="7" y1="0" x2="8" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="12" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="-4" y="0" length="middle"/>
</symbol>
<symbol name="1572-1055-COUT">
<wire x1="0" y1="-2" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2" width="0.254" layer="94"/>
<wire x1="3" y1="2" x2="3" y2="-2" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="0" x2="-1" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-5" y="0" length="middle"/>
<pin name="P$2" x="6" y="0" length="middle" rot="R180"/>
<text x="-1.4" y="0.4" size="1.27" layer="94">+</text>
</symbol>
<symbol name="CPF11K0000FKEE6-R1">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="1.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="1.5" y1="0.9" x2="2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="-1" x2="2.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="2.5" y1="0.9" x2="3" y2="-1" width="0.254" layer="94"/>
<wire x1="3" y1="-1" x2="3.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="3.5" y1="0.9" x2="4" y2="-1" width="0.254" layer="94"/>
<wire x1="4" y1="-1" x2="4.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="4.5" y1="0.9" x2="4.9" y2="0" width="0.254" layer="94"/>
<wire x1="4.9" y1="0" x2="6.1" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-4" y="0" length="middle"/>
<pin name="P$2" x="10" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="CF18JT6K20CT-R2">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="1.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="1.5" y1="0.9" x2="2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="-1" x2="2.6" y2="0.9" width="0.254" layer="94"/>
<wire x1="2.6" y1="0.9" x2="3" y2="-1" width="0.254" layer="94"/>
<wire x1="3" y1="-1" x2="3.5" y2="0.8" width="0.254" layer="94"/>
<wire x1="3.5" y1="0.8" x2="4" y2="0" width="0.254" layer="94"/>
<wire x1="4" y1="0" x2="5" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-4" y="0" length="middle"/>
<pin name="P$2" x="9" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="NF123G-302">
<wire x1="-2" y1="1" x2="-4" y2="1" width="0.254" layer="94"/>
<wire x1="-4" y1="1" x2="-4" y2="-1" width="0.254" layer="94"/>
<wire x1="-4" y1="-1" x2="-2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="1" x2="4" y2="1" width="0.254" layer="94"/>
<wire x1="4" y1="1" x2="4" y2="-1" width="0.254" layer="94"/>
<wire x1="4" y1="-1" x2="2" y2="-1" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.236065625" width="0.254" layer="94"/>
<text x="-1" y="-0.8" size="1.778" layer="94">M</text>
<pin name="P$1" x="-9" y="0" length="middle"/>
<pin name="P$2" x="9" y="0" length="middle" rot="R180"/>
<text x="-3.5" y="-0.6" size="1.27" layer="94">+</text>
<text x="2.7" y="-0.5" size="1.27" layer="94">-</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1572-1039-CIN">
<gates>
<gate name="G$1" symbol="1572-1039-CIN" x="-2" y="0"/>
</gates>
<devices>
<device name="" package="1572-1039-CIN">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N5820">
<gates>
<gate name="G$1" symbol="1N5820" x="-4" y="0"/>
</gates>
<devices>
<device name="" package="1N5820">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M10144">
<gates>
<gate name="G$1" symbol="M10144" x="-4" y="0.1"/>
</gates>
<devices>
<device name="" package="M10144">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1572-1055-COUT">
<gates>
<gate name="G$1" symbol="1572-1055-COUT" x="-0.9" y="0"/>
</gates>
<devices>
<device name="" package="1572-1055-COUT">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPF11K0000KFEE6-R1">
<gates>
<gate name="G$1" symbol="CPF11K0000FKEE6-R1" x="-3.048" y="-0.254"/>
</gates>
<devices>
<device name="" package="CPF11K0000FKEE6-R1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CF18JT6K20CT">
<gates>
<gate name="G$1" symbol="CF18JT6K20CT-R2" x="-2.6" y="-0.2"/>
</gates>
<devices>
<device name="" package="CF18JT6K20CT">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NF123G-302">
<gates>
<gate name="G$1" symbol="NF123G-302" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NF123G-302">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="H-Bridge Library">
<packages>
<package name="SPDT-13">
<wire x1="0" y1="0" x2="8" y2="0" width="0.1524" layer="25"/>
<wire x1="8" y1="0" x2="8" y2="16" width="0.1524" layer="25"/>
<wire x1="8" y1="16" x2="0" y2="16" width="0.1524" layer="25"/>
<wire x1="0" y1="16" x2="0" y2="0" width="0.1524" layer="25"/>
<hole x="2" y="0" drill="1.2"/>
<hole x="5.3" y="0" drill="1.2"/>
<hole x="5.3" y="16" drill="1.2"/>
<hole x="2" y="16" drill="1.2"/>
<pad name="P$1" x="3.6" y="8" drill="1.2"/>
<pad name="P$3" x="3.6" y="12.7" drill="1.2"/>
<pad name="P$2" x="3.6" y="3.3" drill="1.2"/>
<text x="4.8" y="4" size="0.8128" layer="25">1</text>
<text x="4.8" y="8.6" size="0.8128" layer="25">2</text>
<text x="4.8" y="13.3" size="0.8128" layer="25">3</text>
<text x="1.5" y="17.5" size="0.8128" layer="25">SPDT-13</text>
</package>
</packages>
<symbols>
<symbol name="SPDT-13">
<circle x="0" y="0" radius="0.5" width="0.254" layer="94"/>
<circle x="5" y="2" radius="0.5" width="0.254" layer="94"/>
<circle x="5" y="-2" radius="0.5" width="0.254" layer="94"/>
<pin name="P$1" x="-5" y="0" length="middle"/>
<pin name="P$2" x="10" y="-2" length="middle" rot="R180"/>
<pin name="P$3" x="10" y="2" length="middle" rot="R180"/>
<text x="5.7" y="2.4" size="1.27" layer="95">1</text>
<text x="-1.7" y="0.4" size="1.27" layer="95">2</text>
<text x="5.8" y="-1.7" size="1.27" layer="95">3</text>
<wire x1="0" y1="0" x2="5.08" y2="2.032" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPDT-13">
<gates>
<gate name="G$1" symbol="SPDT-13" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SPDT-13">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$3"/>
<connect gate="G$1" pin="P$3" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-11-25_23-05-00_Library">
<packages>
<package name="TO_80AE-GE3_VIS" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.3208"/>
<pad name="2" x="2.6" y="0" drill="0.8128" diameter="1.3208"/>
<pad name="3" x="5.2" y="0" drill="0.8128" diameter="1.3208"/>
<wire x1="-2.5562" y1="-0.0691" x2="-0.9907" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="-2.6832" y1="-2.1265" x2="7.8832" y2="-2.1265" width="0.1524" layer="21"/>
<wire x1="7.8832" y1="-2.1265" x2="7.8832" y2="2.8265" width="0.1524" layer="21"/>
<wire x1="7.8832" y1="2.8265" x2="-2.6832" y2="2.8265" width="0.1524" layer="21"/>
<wire x1="-2.6832" y1="2.8265" x2="-2.6832" y2="-2.1265" width="0.1524" layer="21"/>
<wire x1="0.9907" y1="-0.0691" x2="1.6093" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="3.5907" y1="-0.0691" x2="4.2093" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="6.1907" y1="-0.0691" x2="7.7562" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="-4.3342" y1="0" x2="-4.5882" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-4.5882" y1="0" x2="-4.3342" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.5562" y1="-0.1961" x2="7.7562" y2="-0.1961" width="0.1524" layer="51"/>
<wire x1="-2.5562" y1="-1.9995" x2="7.7562" y2="-1.9995" width="0.1524" layer="51"/>
<wire x1="7.7562" y1="-1.9995" x2="7.7562" y2="2.6995" width="0.1524" layer="51"/>
<wire x1="7.7562" y1="2.6995" x2="-2.5562" y2="2.6995" width="0.1524" layer="51"/>
<wire x1="-2.5562" y1="2.6995" x2="-2.5562" y2="-1.9995" width="0.1524" layer="51"/>
<wire x1="-2.1752" y1="0" x2="-2.4292" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-2.4292" y1="0" x2="-2.1752" y2="0" width="0" layer="51" curve="-180"/>
<text x="1.7356" y="0.9342" size="0.635" layer="51" ratio="4">TAB</text>
<text x="-0.6712" y="-0.285" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="0.8712" y="-0.285" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="TRANS_N-MOSFETN" urn="urn:adsk.eagle:symbol:2/1">
<pin name="D" x="15.24" y="17.78" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="S" x="15.24" y="-12.7" visible="pin" length="short" direction="pas" rot="R90"/>
<pin name="G" x="0" y="0" visible="pin" length="short" direction="pas"/>
<wire x1="2.54" y1="0" x2="4.445" y2="0" width="0.2032" layer="94"/>
<wire x1="12.065" y1="0" x2="12.065" y2="5.08" width="0.2032" layer="94"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="3.175" width="0.2032" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="1.27" width="0.2032" layer="94"/>
<wire x1="12.7" y1="3.81" x2="12.7" y2="5.08" width="0.2032" layer="94"/>
<wire x1="13.97" y1="2.54" x2="14.605" y2="2.54" width="0.2032" layer="94"/>
<wire x1="12.7" y1="0.635" x2="14.605" y2="0.635" width="0.2032" layer="94"/>
<wire x1="14.605" y1="0" x2="14.605" y2="2.54" width="0.2032" layer="94"/>
<wire x1="14.605" y1="0" x2="15.875" y2="0" width="0.2032" layer="94"/>
<wire x1="12.7" y1="4.445" x2="14.605" y2="4.445" width="0.2032" layer="94"/>
<wire x1="15.875" y1="3.175" x2="15.875" y2="5.08" width="0.2032" layer="94"/>
<wire x1="15.875" y1="0" x2="15.875" y2="1.905" width="0.2032" layer="94"/>
<wire x1="16.51" y1="3.175" x2="15.24" y2="3.175" width="0.2032" layer="94"/>
<wire x1="14.605" y1="5.08" x2="15.875" y2="5.08" width="0.2032" layer="94"/>
<wire x1="14.605" y1="4.445" x2="14.605" y2="5.08" width="0.2032" layer="94"/>
<wire x1="4.445" y1="0" x2="12.065" y2="0" width="0.1524" layer="94"/>
<wire x1="15.2654" y1="0" x2="15.24" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="8.89" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="6.35" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="8.89" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="8.89" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-1.905" x2="7.62" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-0.635" x2="6.35" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-0.635" x2="8.89" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-0.635" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.2654" y2="5.08" width="0.1524" layer="94"/>
<wire x1="15.2654" y1="5.08" x2="15.2146" y2="5.08" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2146" y1="5.08" x2="15.2654" y2="5.08" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2654" y1="0" x2="15.2146" y2="0" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2146" y1="0" x2="15.2654" y2="0" width="0.508" layer="94" curve="-180"/>
<wire x1="14.6304" y1="0.635" x2="14.5796" y2="0.635" width="0.508" layer="94" curve="-180"/>
<wire x1="14.5796" y1="0.635" x2="14.6304" y2="0.635" width="0.508" layer="94" curve="-180"/>
<wire x1="25.4" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94" curve="-180"/>
<polygon width="0.0254" layer="94">
<vertex x="13.97" y="3.175"/>
<vertex x="12.7" y="2.54"/>
<vertex x="13.97" y="1.905"/>
</polygon>
<polygon width="0.0254" layer="94">
<vertex x="16.51" y="1.905"/>
<vertex x="15.24" y="1.905"/>
<vertex x="15.875" y="3.175"/>
</polygon>
<text x="29.21" y="8.255" size="2.54" layer="95" ratio="10">&gt;Name</text>
<text x="29.21" y="3.81" size="2.54" layer="96" ratio="10">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIHA11N80AE-GE3" urn="urn:adsk.eagle:component:3/1" prefix="U">
<gates>
<gate name="A" symbol="TRANS_N-MOSFETN" x="0" y="0"/>
</gates>
<devices>
<device name="TO_80AE-GE3_VIS" package="TO_80AE-GE3_VIS">
<connects>
<connect gate="A" pin="D" pad="2"/>
<connect gate="A" pin="G" pad="1"/>
<connect gate="A" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="742-SIHA11N80AE-GE3TR-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="742-SIHA11N80AE-GE3CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_3" value="742-SIHA11N80AE-GE3DKR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SIHA11N80AE-GE3" constant="no"/>
<attribute name="MFR_NAME" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-07_22-06-33_Library">
<packages>
<package name="DIODE_1N5408G-T_DIO" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="1.5494" diameter="2.0574" shape="square"/>
<pad name="2" x="19.9" y="0" drill="1.5494" diameter="2.0574" rot="R180"/>
<wire x1="-2.5527" y1="0" x2="-1.3614" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.9177" y1="0.635" x2="-1.9177" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.0732" y1="-2.7813" x2="14.8268" y2="-2.7813" width="0.1524" layer="21"/>
<wire x1="14.8268" y1="-2.7813" x2="14.8268" y2="2.7813" width="0.1524" layer="21"/>
<wire x1="14.8268" y1="2.7813" x2="5.0732" y2="2.7813" width="0.1524" layer="21"/>
<wire x1="5.0732" y1="2.7813" x2="5.0732" y2="-2.7813" width="0.1524" layer="21"/>
<wire x1="-2.5527" y1="0" x2="-1.2827" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.9177" y1="0.635" x2="-1.9177" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="0" y1="0" x2="5.2002" y2="0" width="0.1524" layer="51"/>
<wire x1="19.9" y1="0" x2="14.6998" y2="0" width="0.1524" layer="51"/>
<wire x1="5.2002" y1="-2.6543" x2="14.6998" y2="-2.6543" width="0.1524" layer="51"/>
<wire x1="14.6998" y1="-2.6543" x2="14.6998" y2="2.6543" width="0.1524" layer="51"/>
<wire x1="14.6998" y1="2.6543" x2="5.2002" y2="2.6543" width="0.1524" layer="51"/>
<wire x1="5.2002" y1="2.6543" x2="5.2002" y2="-2.6543" width="0.1524" layer="51"/>
<text x="6.6788" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="8.2212" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="DIODE" urn="urn:adsk.eagle:symbol:2/1">
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="10.16" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="2.54" y1="0" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="94"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.905" width="0.2032" layer="94"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.2032" layer="94"/>
<wire x1="6.35" y1="0" x2="3.81" y2="1.905" width="0.2032" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="0" width="0.2032" layer="94"/>
<text x="-3.8831" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-2.8148" y="2.7051" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N5408G-T" urn="urn:adsk.eagle:component:4/1" prefix="CR">
<gates>
<gate name="A" symbol="DIODE" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="DIODE_1N5408G-T_DIO" package="DIODE_1N5408G-T_DIO">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="1N5408GDICT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="1N5408GDITR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1N5408G-T" constant="no"/>
<attribute name="MFR_NAME" value="Diodes Inc" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-07_22-12-51_Library">
<packages>
<package name="D8" urn="urn:adsk.eagle:footprint:1/1">
<smd name="1" x="-2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="2" x="-2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="3" x="-2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="4" x="-2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="5" x="2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="6" x="2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="7" x="2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="8" x="2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<wire x1="-1.9939" y1="1.651" x2="-1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.381" x2="-1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.889" x2="-1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.159" x2="-1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-1.651" x2="1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.381" x2="1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.889" x2="1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.159" x2="1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.5019" x2="1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.5019" x2="1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.5019" x2="-1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.5019" x2="-1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5019" x2="-0.3048" y2="2.5019" width="0.1524" layer="51" curve="-180"/>
<text x="-3.2941" y="2.3368" size="1.27" layer="51" ratio="6">*</text>
<wire x1="-1.3737" y1="-2.5019" x2="1.3737" y2="-2.5019" width="0.1524" layer="21"/>
<wire x1="1.3737" y1="2.5019" x2="-1.3737" y2="2.5019" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5019" x2="-0.3048" y2="2.5019" width="0.1524" layer="21" curve="-180"/>
<text x="-3.2941" y="2.3368" size="1.27" layer="21" ratio="6">*</text>
<text x="-3.2712" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="-1.7288" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
<package name="D8-M" urn="urn:adsk.eagle:footprint:3/1">
<smd name="1" x="-2.8257" y="1.905" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="2" x="-2.8257" y="0.635" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="3" x="-2.8257" y="-0.635" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="4" x="-2.8257" y="-1.905" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="5" x="2.8257" y="-1.905" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="6" x="2.8257" y="-0.635" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="7" x="2.8257" y="0.635" dx="1.6637" dy="0.5588" layer="1"/>
<smd name="8" x="2.8257" y="1.905" dx="1.6637" dy="0.5588" layer="1"/>
<wire x1="-1.9939" y1="1.651" x2="-1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.381" x2="-1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.889" x2="-1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.159" x2="-1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-1.651" x2="1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.381" x2="1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.889" x2="1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.159" x2="1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.5019" x2="1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.5019" x2="1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.5019" x2="-1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.5019" x2="-1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5019" x2="-0.3048" y2="2.5019" width="0.1524" layer="51" curve="-180"/>
<text x="-2.1892" y="1.1557" size="1.27" layer="51" ratio="6">*</text>
<wire x1="-2.1209" y1="-2.6289" x2="2.1209" y2="-2.6289" width="0.1524" layer="21"/>
<wire x1="2.1209" y1="2.6289" x2="-2.1209" y2="2.6289" width="0.1524" layer="21"/>
<text x="-3.656" y="2.3368" size="1.27" layer="21" ratio="6">*</text>
<text x="-3.2712" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="-1.7288" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
<package name="D8-L" urn="urn:adsk.eagle:footprint:2/1">
<smd name="1" x="-2.6226" y="1.905" dx="1.2573" dy="0.508" layer="1"/>
<smd name="2" x="-2.6226" y="0.635" dx="1.2573" dy="0.508" layer="1"/>
<smd name="3" x="-2.6226" y="-0.635" dx="1.2573" dy="0.508" layer="1"/>
<smd name="4" x="-2.6226" y="-1.905" dx="1.2573" dy="0.508" layer="1"/>
<smd name="5" x="2.6226" y="-1.905" dx="1.2573" dy="0.508" layer="1"/>
<smd name="6" x="2.6226" y="-0.635" dx="1.2573" dy="0.508" layer="1"/>
<smd name="7" x="2.6226" y="0.635" dx="1.2573" dy="0.508" layer="1"/>
<smd name="8" x="2.6226" y="1.905" dx="1.2573" dy="0.508" layer="1"/>
<wire x1="-1.9939" y1="1.651" x2="-1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.381" x2="-1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.889" x2="-1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.159" x2="-1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-1.651" x2="1.9939" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="1.9939" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.381" x2="1.9939" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="1.9939" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.889" x2="1.9939" y2="0.381" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="1.9939" y2="0.889" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.159" x2="1.9939" y2="1.651" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="1.9939" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="-2.5019" x2="1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="-2.5019" x2="1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="1.9939" y1="2.5019" x2="-1.9939" y2="2.5019" width="0.1524" layer="51"/>
<wire x1="-1.9939" y1="2.5019" x2="-1.9939" y2="-2.5019" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5019" x2="-0.3048" y2="2.5019" width="0.1524" layer="51" curve="-180"/>
<text x="-2.1892" y="1.1557" size="1.27" layer="51" ratio="6">*</text>
<wire x1="-2.1209" y1="-2.6289" x2="2.1209" y2="-2.6289" width="0.1524" layer="21"/>
<wire x1="2.1209" y1="2.6289" x2="-2.1209" y2="2.6289" width="0.1524" layer="21"/>
<text x="-3.4528" y="2.286" size="1.27" layer="21" ratio="6">*</text>
<text x="-3.2712" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="-1.7288" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="NA555_D_8" urn="urn:adsk.eagle:symbol:4/1">
<pin name="GND" x="2.54" y="0" length="middle" direction="pwr"/>
<pin name="TRIG" x="2.54" y="-2.54" length="middle" direction="pas"/>
<pin name="OUT" x="2.54" y="-5.08" length="middle" direction="out"/>
<pin name="RESET" x="2.54" y="-7.62" length="middle" direction="pas"/>
<pin name="CONT" x="63.5" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="THRES" x="63.5" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="DISCH" x="63.5" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="VCC" x="63.5" y="0" length="middle" direction="pwr" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="58.42" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="58.42" y2="5.08" width="0.1524" layer="94"/>
<wire x1="58.42" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="28.2946" y="9.1186" size="2.083" layer="95" ratio="6">&gt;Name</text>
<text x="27.6552" y="6.5786" size="2.083" layer="96" ratio="6">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NA555D" urn="urn:adsk.eagle:component:5/1" prefix="U">
<gates>
<gate name="A" symbol="NA555_D_8" x="0" y="0"/>
</gates>
<devices>
<device name="D8" package="D8">
<connects>
<connect gate="A" pin="CONT" pad="5"/>
<connect gate="A" pin="DISCH" pad="7"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="RESET" pad="4"/>
<connect gate="A" pin="THRES" pad="6"/>
<connect gate="A" pin="TRIG" pad="2"/>
<connect gate="A" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="NA555D-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-NA555D-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NA555D" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="D8-M" package="D8-M">
<connects>
<connect gate="A" pin="CONT" pad="5"/>
<connect gate="A" pin="DISCH" pad="7"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="RESET" pad="4"/>
<connect gate="A" pin="THRES" pad="6"/>
<connect gate="A" pin="TRIG" pad="2"/>
<connect gate="A" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="NA555D-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-NA555D-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NA555D" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="D8-L" package="D8-L">
<connects>
<connect gate="A" pin="CONT" pad="5"/>
<connect gate="A" pin="DISCH" pad="7"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="RESET" pad="4"/>
<connect gate="A" pin="THRES" pad="6"/>
<connect gate="A" pin="TRIG" pad="2"/>
<connect gate="A" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="NA555D-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-NA555D-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="NA555D" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-08_23-05-30_Library">
<packages>
<package name="CAP_UKL_5X11_NCH" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.762" diameter="1.27"/>
<pad name="2" x="2" y="0" drill="0.762" diameter="1.27" rot="R180"/>
<wire x1="-3.0259" y1="0" x2="-1.7559" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.3909" y1="0.635" x2="-2.3909" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.6289" y1="0" x2="-1.6289" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.6289" y1="0" x2="3.6289" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.0259" y1="0" x2="-1.7559" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.3909" y1="0.635" x2="-2.3909" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.5019" y1="0" x2="-1.5019" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.5019" y1="0" x2="3.5019" y2="0" width="0" layer="51" curve="-180"/>
<text x="-2.2712" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="-0.7288" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CAPH" urn="urn:adsk.eagle:symbol:2/1">
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="3.4798" y1="-1.905" x2="3.4798" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="-1.905" x2="4.1148" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="0" x2="5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="0.9525" x2="1.5875" y2="0.9525" width="0.1524" layer="94"/>
<text x="-5.1531" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-4.0848" y="2.0701" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="UKL1HR15MDD1TD" urn="urn:adsk.eagle:component:4/1" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="CAP_UKL_5X11_NCH" package="CAP_UKL_5X11_NCH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="493-10510-1-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="493-10510-3-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UKL1HR15MDD1TD" constant="no"/>
<attribute name="MFR_NAME" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-08_23-18-02_Library">
<packages>
<package name="RN60_VIS" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.9398" diameter="1.4478" shape="square"/>
<pad name="2" x="8.4074" y="0" drill="0.9398" diameter="1.4478" rot="R180"/>
<wire x1="-1.3208" y1="-2.159" x2="9.7282" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="9.7282" y1="-2.159" x2="9.7282" y2="2.159" width="0.1524" layer="21"/>
<wire x1="9.7282" y1="2.159" x2="-1.3208" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-1.3208" y1="2.159" x2="-1.3208" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="-1.1938" y2="0" width="0.1524" layer="51"/>
<wire x1="8.4074" y1="0" x2="9.6012" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.1938" y1="-2.032" x2="9.6012" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="9.6012" y1="-2.032" x2="9.6012" y2="2.032" width="0.1524" layer="51"/>
<wire x1="9.6012" y1="2.032" x2="-1.1938" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-1.1938" y1="2.032" x2="-1.1938" y2="-2.032" width="0.1524" layer="51"/>
<text x="0.9325" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="2.4749" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RES" urn="urn:adsk.eagle:symbol:2/1">
<pin name="1" x="0" y="0" visible="pin" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="12.7" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="1.27" width="0.2032" layer="94"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="1.27" width="0.2032" layer="94"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.175" y2="1.27" width="0.2032" layer="94"/>
<wire x1="9.525" y1="-1.27" x2="10.16" y2="0" width="0.2032" layer="94"/>
<text x="-2.6131" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-2.1798" y="2.0701" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RN60D3001FRE6" urn="urn:adsk.eagle:component:4/1" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="RN60_VIS" package="RN60_VIS">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="RN60D3001FRE6-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="541-RN60D3001FRE6CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_3" value="541-RN60D3001FRE6TR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RN60D3001FRE6" constant="no"/>
<attribute name="MFR_NAME" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-08_23-11-41_Library">
<packages>
<package name="RES_CPF1_VIS" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.9398" diameter="1.4478" shape="square"/>
<pad name="2" x="13.462" y="0" drill="0.9398" diameter="1.4478" rot="R180"/>
<wire x1="2.667" y1="-1.3716" x2="10.795" y2="-1.3716" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.3716" x2="10.795" y2="1.3716" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.3716" x2="2.667" y2="1.3716" width="0.1524" layer="21"/>
<wire x1="2.667" y1="1.3716" x2="2.667" y2="-1.3716" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="2.794" y2="0" width="0.1524" layer="51"/>
<wire x1="13.462" y1="0" x2="10.668" y2="0" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-1.2446" x2="10.668" y2="-1.2446" width="0.1524" layer="51"/>
<wire x1="10.668" y1="-1.2446" x2="10.668" y2="1.2446" width="0.1524" layer="51"/>
<wire x1="10.668" y1="1.2446" x2="2.794" y2="1.2446" width="0.1524" layer="51"/>
<wire x1="2.794" y1="1.2446" x2="2.794" y2="-1.2446" width="0.1524" layer="51"/>
<text x="3.4598" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="5.0022" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RES" urn="urn:adsk.eagle:symbol:2/1">
<pin name="1" x="0" y="0" visible="pin" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="12.7" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="1.27" width="0.2032" layer="94"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="1.27" width="0.2032" layer="94"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.175" y2="1.27" width="0.2032" layer="94"/>
<wire x1="9.525" y1="-1.27" x2="10.16" y2="0" width="0.2032" layer="94"/>
<text x="-2.6131" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-2.1798" y="2.0701" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CPF14K7000JKEE6" urn="urn:adsk.eagle:component:4/1" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="RES_CPF1_VIS" package="RES_CPF1_VIS">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="CPF4.70KDCT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="CPF4.70KDTR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CPF14K7000JKEE6" constant="no"/>
<attribute name="MFR_NAME" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-12-08_23-35-38_Library">
<packages>
<package name="CAP_UEP_5X11_NCH" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.762" diameter="1.27"/>
<pad name="2" x="2" y="0" drill="0.762" diameter="1.27" rot="R180"/>
<wire x1="-3.0259" y1="0" x2="-1.7559" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.3909" y1="0.635" x2="-2.3909" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.6289" y1="0" x2="-1.6289" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.6289" y1="0" x2="3.6289" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.0259" y1="0" x2="-1.7559" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.3909" y1="0.635" x2="-2.3909" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.5019" y1="0" x2="-1.5019" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.5019" y1="0" x2="3.5019" y2="0" width="0" layer="51" curve="-180"/>
<text x="-2.2712" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="-0.7288" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CAPH" urn="urn:adsk.eagle:symbol:2/1">
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="3.4798" y1="-1.905" x2="3.4798" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="-1.905" x2="4.1148" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="0" x2="5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="0.9525" x2="1.5875" y2="0.9525" width="0.1524" layer="94"/>
<text x="-5.1531" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-4.0848" y="2.0701" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="UKT1H0R1MDD1TD" urn="urn:adsk.eagle:component:4/1" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="CAP_UEP_5X11_NCH" package="CAP_UEP_5X11_NCH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="493-10656-1-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="493-10656-3-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UKT1H0R1MDD1TD" constant="no"/>
<attribute name="MFR_NAME" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="G1" library="battery" library_urn="urn:adsk.eagle:library:109" deviceset="10MM_SM_COIN_CELL_CLIP" device="" package3d_urn="urn:adsk.eagle:package:4612/1"/>
<part name="U1" library="LM2576T" deviceset="LM2576T-ADJ/LF03" device=""/>
<part name="U$1" library="Lab6_Library" deviceset="1572-1039-CIN" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="U$2" library="Lab6_Library" deviceset="1N5820" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="U$3" library="Lab6_Library" deviceset="M10144" device=""/>
<part name="U$4" library="Lab6_Library" deviceset="1572-1055-COUT" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="U$5" library="Lab6_Library" deviceset="CPF11K0000KFEE6-R1" device=""/>
<part name="U$6" library="Lab6_Library" deviceset="CF18JT6K20CT" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="U$7" library="H-Bridge Library" deviceset="SPDT-13" device=""/>
<part name="U2" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U3" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U4" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U5" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U$8" library="Lab6_Library" deviceset="NF123G-302" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="CR1" library="2021-12-07_22-06-33_Library" deviceset="1N5408G-T" device="DIODE_1N5408G-T_DIO"/>
<part name="CR2" library="2021-12-07_22-06-33_Library" deviceset="1N5408G-T" device="DIODE_1N5408G-T_DIO"/>
<part name="U6" library="2021-12-07_22-12-51_Library" deviceset="NA555D" device="D8"/>
<part name="GND9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="C1" library="2021-12-08_23-05-30_Library" deviceset="UKL1HR15MDD1TD" device="CAP_UKL_5X11_NCH"/>
<part name="GND10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="R1" library="2021-12-08_23-18-02_Library" deviceset="RN60D3001FRE6" device="RN60_VIS"/>
<part name="R2" library="2021-12-08_23-11-41_Library" deviceset="CPF14K7000JKEE6" device="RES_CPF1_VIS"/>
<part name="C2" library="2021-12-08_23-35-38_Library" deviceset="UKT1H0R1MDD1TD" device="CAP_UEP_5X11_NCH"/>
<part name="GND11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="90" y="-88" size="1.778" layer="94">make input from DC motor go through 555 to h-bridge</text>
<text x="64.1" y="-35.5" size="1.27" layer="95">open</text>
<text x="120.4" y="-54.7" size="1.778" layer="95">7</text>
<text x="97.4" y="-55" size="1.778" layer="95">4</text>
<text x="133.6" y="-54.3" size="1.778" layer="95">6</text>
<text x="63" y="-46" size="1.778" layer="95">3</text>
</plain>
<instances>
<instance part="GND1" gate="G$1" x="-8" y="-17.6" smashed="yes">
<attribute name="VALUE" x="-8" y="-19.378" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="G1" gate="G$1" x="-8" y="-4" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.73" y="-4.175" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="-9.73" y="-5.92" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="U1" gate="A" x="6" y="6" smashed="yes">
<attribute name="NAME" x="34.2956" y="15.1186" size="1.27" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="33.6606" y="12.5786" size="1.27" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U$1" gate="G$1" x="0" y="-4" smashed="yes" rot="R270"/>
<instance part="GND2" gate="G$1" x="0" y="-21" smashed="yes">
<attribute name="VALUE" x="0" y="-22.778" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="U$2" gate="G$1" x="8" y="-27" smashed="yes" rot="R90"/>
<instance part="GND3" gate="G$1" x="8" y="-35" smashed="yes">
<attribute name="VALUE" x="8" y="-36.778" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="U$3" gate="G$1" x="12" y="-13" smashed="yes"/>
<instance part="U$4" gate="G$1" x="41" y="-18" smashed="yes" rot="R270"/>
<instance part="GND4" gate="G$1" x="41" y="-29" smashed="yes">
<attribute name="VALUE" x="41" y="-30.778" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="GND5" gate="G$1" x="69.5" y="-4.2" smashed="yes">
<attribute name="VALUE" x="69.5" y="-5.978" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="GND6" gate="G$1" x="85" y="-2" smashed="yes">
<attribute name="VALUE" x="85" y="-3.778" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="U$5" gate="G$1" x="67" y="-13" smashed="yes"/>
<instance part="U$6" gate="G$1" x="85" y="-13" smashed="yes"/>
<instance part="GND7" gate="G$1" x="98" y="-25" smashed="yes">
<attribute name="VALUE" x="98" y="-26.778" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="U$7" gate="G$1" x="182.42" y="-50.74" smashed="yes" rot="R270"/>
<instance part="U2" gate="A" x="147.46" y="-82.02" smashed="yes">
<attribute name="NAME" x="146.67" y="-84.765" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.67" y="-87.21" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U3" gate="A" x="216" y="-77.1" smashed="yes" rot="R180">
<attribute name="NAME" x="217.79" y="-80.355" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="245.79" y="-77.91" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U4" gate="A" x="147.16" y="-115.86" smashed="yes">
<attribute name="NAME" x="141.37" y="-113.605" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="115.37" y="-116.05" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U5" gate="A" x="216.38" y="-110.94" smashed="yes" rot="R180">
<attribute name="NAME" x="218.17" y="-108.195" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="245.17" y="-105.75" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U$8" gate="G$1" x="181.08" y="-96" smashed="yes"/>
<instance part="GND8" gate="G$1" x="180.66" y="-135.78" smashed="yes">
<attribute name="VALUE" x="180.66" y="-138.558" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="CR1" gate="A" x="165.48" y="-131.52" smashed="yes">
<attribute name="VALUE" x="149.5969" y="-136.0699" size="1.27" layer="96" ratio="10"/>
<attribute name="NAME" x="163.6652" y="-133.8149" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="CR2" gate="A" x="195.08" y="-131.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="213.9631" y="-134.1301" size="1.27" layer="96" ratio="10" rot="R180"/>
<attribute name="NAME" x="197.8948" y="-132.3851" size="1.27" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="U6" gate="A" x="132.32" y="-43.74" smashed="yes" rot="R180">
<attribute name="NAME" x="104.0254" y="-36.8586" size="1.27" layer="95" ratio="6" rot="R180"/>
<attribute name="VALUE" x="103.6648" y="-34.3186" size="1.27" layer="96" ratio="6" rot="R180"/>
</instance>
<instance part="GND9" gate="G$1" x="129.54" y="-48.26" smashed="yes">
<attribute name="VALUE" x="129.54" y="-50.038" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C1" gate="A" x="137" y="-58" smashed="yes" rot="R270">
<attribute name="VALUE" x="101.8469" y="-64.5499" size="1.27" layer="96" ratio="10"/>
<attribute name="NAME" x="131.9152" y="-62.9299" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="GND10" gate="G$1" x="137" y="-69" smashed="yes">
<attribute name="VALUE" x="137" y="-70.778" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R1" gate="A" x="120" y="-56" smashed="yes">
<attribute name="VALUE" x="108.3869" y="-59.5499" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R2" gate="A" x="97" y="-56" smashed="yes">
<attribute name="VALUE" x="91.3869" y="-61.5499" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C2" gate="A" x="93" y="-56" smashed="yes" rot="R180">
<attribute name="VALUE" x="105.1531" y="-50.4501" size="1.27" layer="96" ratio="10" rot="R180"/>
<attribute name="NAME" x="93.0848" y="-53.0701" size="1.27" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="GND11" gate="G$1" x="85" y="-61" smashed="yes">
<attribute name="VALUE" x="85" y="-62.778" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-85" y="-194" smashed="yes">
<attribute name="DRAWING_NAME" x="259.17" y="-178.76" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="259.17" y="-183.84" size="2.286" layer="94"/>
<attribute name="SHEET" x="272.505" y="-188.92" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<pinref part="GND1" gate="G$1" pin="3.3V"/>
<pinref part="G1" gate="G$1" pin="-"/>
<wire x1="-8" y1="-15.06" x2="-8" y2="-9.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="G$1" pin="3.3V"/>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<wire x1="0" y1="-18.46" x2="0" y2="-12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="G$1" pin="3.3V"/>
<pinref part="U$2" gate="G$1" pin="P$1"/>
<wire x1="8" y1="-32.46" x2="8" y2="-29" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="G$1" pin="3.3V"/>
<pinref part="U$4" gate="G$1" pin="P$2"/>
<wire x1="41" y1="-26.46" x2="41" y2="-24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="G$1" pin="3.3V"/>
<pinref part="U1" gate="A" pin="GND"/>
<wire x1="69.5" y1="-1.66" x2="69.5" y2="0.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="~ON~/OFF"/>
<wire x1="69.5" y1="6" x2="85" y2="6" width="0.1524" layer="91"/>
<pinref part="GND6" gate="G$1" pin="3.3V"/>
<wire x1="85" y1="6" x2="85" y2="0.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<pinref part="GND7" gate="G$1" pin="3.3V"/>
<wire x1="94" y1="-13" x2="98" y2="-13" width="0.1524" layer="91"/>
<wire x1="98" y1="-13" x2="98" y2="-22.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="G$1" pin="3.3V"/>
<pinref part="U6" gate="A" pin="GND"/>
<wire x1="129.54" y1="-45.72" x2="129.54" y2="-43.74" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-43.74" x2="129.78" y2="-43.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="G$1" pin="3.3V"/>
<pinref part="C2" gate="A" pin="2"/>
<wire x1="85" y1="-58.46" x2="85" y2="-56" width="0.1524" layer="91"/>
<wire x1="85" y1="-56" x2="85.38" y2="-56" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<pinref part="U$4" gate="G$1" pin="P$1"/>
<wire x1="24" y1="-13" x2="41" y2="-13" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="P$1"/>
<wire x1="41" y1="-13" x2="63" y2="-13" width="0.1524" layer="91"/>
<junction x="41" y="-13"/>
<pinref part="U6" gate="A" pin="VCC"/>
<wire x1="63" y1="-13" x2="63" y2="-43.74" width="0.1524" layer="91"/>
<wire x1="63" y1="-43.74" x2="68.82" y2="-43.74" width="0.1524" layer="91"/>
<junction x="63" y="-13"/>
<wire x1="68.82" y1="-43.74" x2="68.82" y2="-56" width="0.1524" layer="91"/>
<wire x1="68.82" y1="-56" x2="85.38" y2="-56" width="0.1524" layer="91"/>
<junction x="68.82" y="-43.74"/>
<junction x="85.38" y="-56"/>
</segment>
<segment>
<pinref part="CR2" gate="A" pin="1"/>
<pinref part="GND8" gate="G$1" pin="3.3V"/>
<wire x1="184.92" y1="-131.68" x2="180.66" y2="-131.68" width="0.1524" layer="91"/>
<wire x1="180.66" y1="-131.68" x2="180.66" y2="-133.24" width="0.1524" layer="91"/>
<pinref part="CR1" gate="A" pin="1"/>
<wire x1="175.64" y1="-131.52" x2="180.66" y2="-131.52" width="0.1524" layer="91"/>
<wire x1="180.66" y1="-131.52" x2="180.66" y2="-133.24" width="0.1524" layer="91"/>
<junction x="180.66" y="-133.24"/>
</segment>
<segment>
<pinref part="GND10" gate="G$1" pin="3.3V"/>
<pinref part="C1" gate="A" pin="2"/>
<wire x1="137" y1="-66.46" x2="137" y2="-65.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="G1" gate="G$1" pin="+"/>
<wire x1="-8" y1="1.08" x2="-8" y2="6" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VIN"/>
<wire x1="-8" y1="6" x2="0" y2="6" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="0" y1="6" x2="8.54" y2="6" width="0.1524" layer="91"/>
<wire x1="0" y1="-1" x2="0" y2="6" width="0.1524" layer="91"/>
<junction x="0" y="6"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$2"/>
<wire x1="8" y1="-17" x2="8" y2="-13" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="OUT"/>
<wire x1="8" y1="-13" x2="8" y2="3.5" width="0.1524" layer="91"/>
<wire x1="8" y1="3.5" x2="8.54" y2="3.46" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<junction x="8" y="-13"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="P$2"/>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<wire x1="77" y1="-13" x2="79" y2="-13" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="FB"/>
<wire x1="79" y1="-13" x2="81" y2="-13" width="0.1524" layer="91"/>
<wire x1="69.5" y1="3.46" x2="79" y2="3.46" width="0.1524" layer="91"/>
<wire x1="79" y1="3.46" x2="79" y2="-13" width="0.1524" layer="91"/>
<junction x="79" y="-13"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U6" gate="A" pin="TRIG"/>
<wire x1="129.78" y1="-41.2" x2="137" y2="-41.2" width="0.1524" layer="91"/>
<wire x1="137" y1="-41.2" x2="137" y2="-56" width="0.1524" layer="91"/>
<pinref part="C1" gate="A" pin="1"/>
<wire x1="137" y1="-56" x2="137" y2="-58" width="0.1524" layer="91"/>
<pinref part="R1" gate="A" pin="2"/>
<wire x1="137" y1="-56" x2="132.7" y2="-56" width="0.1524" layer="91"/>
<junction x="137" y="-56"/>
<wire x1="132.7" y1="-56" x2="133" y2="-56" width="0.1524" layer="91"/>
<wire x1="133" y1="-56" x2="133" y2="-53" width="0.1524" layer="91"/>
<junction x="132.7" y="-56"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R1" gate="A" pin="1"/>
<pinref part="R2" gate="A" pin="2"/>
<wire x1="120" y1="-56" x2="109.7" y2="-56" width="0.1524" layer="91"/>
<wire x1="120" y1="-56" x2="120" y2="-54" width="0.1524" layer="91"/>
<junction x="120" y="-56"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R2" gate="A" pin="1"/>
<wire x1="97" y1="-56" x2="97" y2="-53" width="0.1524" layer="91"/>
<pinref part="C2" gate="A" pin="1"/>
<wire x1="93" y1="-56" x2="97" y2="-56" width="0.1524" layer="91"/>
<junction x="97" y="-56"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="65" y1="-43.7" x2="65" y2="-43.8" width="0.1524" layer="91"/>
<junction x="65" y="-43.8"/>
<wire x1="65" y1="-43.8" x2="65" y2="-46.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="P$1"/>
<pinref part="U4" gate="A" pin="D"/>
<wire x1="172.08" y1="-96" x2="162.4" y2="-96" width="0.1524" layer="91"/>
<wire x1="162.4" y1="-96" x2="162.4" y2="-98.08" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="S"/>
<wire x1="172.08" y1="-96" x2="162.7" y2="-96" width="0.1524" layer="91"/>
<wire x1="162.7" y1="-96" x2="162.7" y2="-94.72" width="0.1524" layer="91"/>
<junction x="172.08" y="-96"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="P$2"/>
<pinref part="U5" gate="A" pin="S"/>
<wire x1="190.08" y1="-96" x2="201.14" y2="-96" width="0.1524" layer="91"/>
<wire x1="201.14" y1="-96" x2="201.14" y2="-98.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="D"/>
<wire x1="190.08" y1="-96" x2="200.76" y2="-96" width="0.1524" layer="91"/>
<wire x1="200.76" y1="-96" x2="200.76" y2="-94.88" width="0.1524" layer="91"/>
<junction x="190.08" y="-96"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U5" gate="A" pin="D"/>
<pinref part="CR2" gate="A" pin="2"/>
<wire x1="201.14" y1="-128.72" x2="201.14" y2="-131.68" width="0.1524" layer="91"/>
<wire x1="201.14" y1="-131.68" x2="195.08" y2="-131.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="CR1" gate="A" pin="2"/>
<pinref part="U4" gate="A" pin="S"/>
<wire x1="165.48" y1="-131.52" x2="162.4" y2="-131.52" width="0.1524" layer="91"/>
<wire x1="162.4" y1="-131.52" x2="162.4" y2="-128.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U3" gate="A" pin="S"/>
<pinref part="U$7" gate="G$1" pin="P$3"/>
<wire x1="200.76" y1="-64.4" x2="184.42" y2="-64.4" width="0.1524" layer="91"/>
<wire x1="184.42" y1="-64.4" x2="184.42" y2="-60.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$2"/>
<pinref part="U2" gate="A" pin="D"/>
<wire x1="180.42" y1="-60.74" x2="180.42" y2="-64.24" width="0.1524" layer="91"/>
<wire x1="180.42" y1="-64.24" x2="162.7" y2="-64.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U4" gate="A" pin="G"/>
<wire x1="147.16" y1="-115.86" x2="147.16" y2="-147" width="0.1524" layer="91"/>
<wire x1="147.16" y1="-147" x2="231" y2="-147" width="0.1524" layer="91"/>
<wire x1="231" y1="-147" x2="231" y2="-77" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="G"/>
<wire x1="231" y1="-77" x2="216" y2="-77" width="0.1524" layer="91"/>
<wire x1="216" y1="-77" x2="216" y2="-77.1" width="0.1524" layer="91"/>
<wire x1="182.4" y1="-43" x2="216" y2="-43" width="0.1524" layer="91"/>
<wire x1="216" y1="-43" x2="216" y2="-77.1" width="0.1524" layer="91"/>
<junction x="216" y="-77.1"/>
<junction x="182.4" y="-43"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U5" gate="A" pin="G"/>
<wire x1="216.38" y1="-110.94" x2="216.38" y2="-144" width="0.1524" layer="91"/>
<wire x1="216.38" y1="-144" x2="140" y2="-144" width="0.1524" layer="91"/>
<wire x1="140" y1="-144" x2="140" y2="-82" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="G"/>
<wire x1="140" y1="-82" x2="147.46" y2="-82" width="0.1524" layer="91"/>
<wire x1="147.46" y1="-82" x2="147.46" y2="-82.02" width="0.1524" layer="91"/>
<wire x1="182" y1="-43" x2="147.46" y2="-43" width="0.1524" layer="91"/>
<wire x1="147.46" y1="-43" x2="147.46" y2="-82.02" width="0.1524" layer="91"/>
<junction x="147.46" y="-82.02"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U6" gate="A" pin="OUT"/>
<pinref part="U$7" gate="G$1" pin="P$1"/>
<wire x1="129.78" y1="-38.66" x2="182.42" y2="-38.66" width="0.1524" layer="91"/>
<wire x1="182.42" y1="-38.66" x2="182.42" y2="-45.74" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
