## Final
India Spott
## cruzID
espott
## Date
12/9/2021
## Files in Repository
BoM Final.xlsx, espott-final-assignment.sch, espott-final-assignment.pro, espott-final-assignment.brd, README
libraries:
ul_1N5408G-T
ul_CPF14K7000JKEE6
ul_LM2576T-ADJ-LF03
ul_NA555D
ul_RN60D3001FRE6
ul_SIHA11N80AE-GE3
ul_UKL1HR15MDD1TD
ul_UKT1H0R1MDD1TD
2021-11-25_23-05-00_Library
2021-12-08_23-05-30_Library
espott-VoltageDividerLib
H-Bridge_Library
Lab6_Library
## Project Function
The final project for CRSN-151C is a combination of labs 6, 7, 8, and 9 where we put together our buck converter, h-bridge, and 555 timer configurations to create one large schematic. The buck converter converts the voltage from a 12V input to a 9V output, which then goes through the 555 timer, only to go to the h-bridge which controls which direction the current is flowing to then control which direction the DC motor rotates. 
